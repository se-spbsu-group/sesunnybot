// TODO: only storage purpose - later switch to smth
import express from 'express';
import bodyParser from 'body-parser';
import * as fs from 'fs';
import { join } from 'path';
import data from '../data/data.json';

const app = express();

app.use(bodyParser.json());

// TODO: remove set as temp storage
const hashtags = new Set(data.hashtags);

// TODO: create norm persistent solution.
const saveHashtagsToJson = (hashtags) => {
    // TODO: error handling, logging
    fs.writeFile(
        join(__dirname, '../data/data.json'),
        JSON.stringify({ hashtags }),
        'utf8',
        (error) => console.log(error)
    );
};

app.get('/hashtags', (req, res) => {
    const hashtagsAsArray = Array.from(hashtags);
    res.send({ hashtags: hashtagsAsArray.length ? hashtagsAsArray : ['empty'] });
});

app.post('/addhashtag', (req, res) => {
    hashtags.add(req.body.hashtag);
    const hashtagsAsArray = Array.from(hashtags);
    res.send({ hashtags: hashtagsAsArray.length ? hashtagsAsArray : ['empty'] });
    saveHashtagsToJson(hashtagsAsArray);
});

app.post('/rmhashtag', (req, res) => {
    hashtags.delete(req.body.hashtag);
    const hashtagsAsArray = Array.from(hashtags);
    res.send({ hashtags: hashtagsAsArray.length ? hashtagsAsArray : ['empty'] });
    saveHashtagsToJson(hashtagsAsArray);
});

app.listen(process.env.WEB_PORT, () => {
    console.log('Storage is up!');
});
