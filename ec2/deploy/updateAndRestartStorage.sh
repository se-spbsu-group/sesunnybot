#!/bin/bash

# any future command that fails will exit the script
set -e
rm -rf /home/ubuntu/sesunnybot
git clone https://gitlab.com/alkostryukov/sesunnybot

cd /home/ubuntu/sesunnybot
echo "Running npm install"
npm install

#Restart the node server
cd /home/ubuntu/
./start.sh
